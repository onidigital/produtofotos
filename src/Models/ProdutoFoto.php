<?php

namespace Onicmspack\ProdutoFotos\Models;

use Illuminate\Database\Eloquent\Model;

class ProdutoFoto extends Model
{
    //
  protected $table = 'produtos_fotos';
    protected $fillable = [
                           'arquivo_id',
                           'produto_id',
                           'principal',
    ];

    public function arquivos()
    {
      return $this->hasMany('Onicmspack\ProdutoFotos\Models\ProdutoFoto');
    }
    public function arquivo()
    {
        return $this->belongsTo('Onicmspack\Arquivos\Models\Arquivo', 'imagem');
    }

   
}