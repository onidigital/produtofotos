<?php 

namespace Onicmspack\ProdutoFotos;

use Illuminate\Support\ServiceProvider;

class ProdutoFotosServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        // de onde carregar as views?
        $this->loadViewsFrom(__DIR__.'/views/produtofotos', 'produtos');

        // Publicando os arquivos:
        // Views
        $this->publishes([
            __DIR__.'/views' => base_path('resources/views/admin/vendor'),
        ], 'views');

       // Migrations
        $this->publishes([
            __DIR__.'/migrations' => base_path('database/migrations/'),
        ], 'migrations');
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        include __DIR__.'/routes.php';
        $this->app->make('Onicmspack\ProdutoFotos\ProdutoFotosController');
    }
}
