<?php

Route::group(['middleware' => ['web','auth', 'authorization'], 'prefix' => 'admin' ], function () {
    // Menu do slide:
    Route::resource('produtos_fotos', 'Onicmspack\ProdutoFotos\ProdutoFotosController');
    Route::post('produtos_fotos/{id}/atualizar_status','Onicmspack\ProdutoFotos\ProdutoFotosController@atualizar_status'); // Atualizar Status Ajax

    Route::get( '/produtos_fotos/marcar_capa/{foto_id}', [ 
			'as'   => 'produto_capa',
			'uses' => 'Admin\ProdutoFotosController@marcar_capa'
			]
		);

});