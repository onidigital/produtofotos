<?php
namespace Onicmspack\ProdutoFotos;

use Onicmspack\ProdutoFotos\Models\ProdutoFoto as ProdutoFoto;
use Onicmspack\Categorias\Models\Categoria as Categoria;
use Onicmspack\Atributos\Models\Atributo as Atributo;
use Onicmspack\Marcas\Models\Marca as Marca;

use Onicmspack\Arquivos\Models\Arquivo as Arquivo;
use Onicmspack\ProdutoFotos\Requests\ProdutoFotosRequest as ProdutoFotosRequest;
use Onicms\Http\Controllers\Controller;

 
class ProdutoFotosController extends Controller
{
    public $caminho = 'admin/produtosfotos/';
    public $views   = 'admin/vendor/produtosfotos/';
    public $titulo  = 'ProdutosFotos';
    
    public $categorias;
    public $marcas;
    public $atributos;
    public $atributos_array = array();
    public $atributos_string;

    public function inicia_models()
    {
        // Categorias para popular o select em create e edit:
        $this->categorias = ['' => '-- Selecione a categoria --'] + Categoria::orderBy('nome')->lists('nome','id')->toArray();
        $this->marcas = ['' => '-- Selecione a marca --'] + Marca::orderBy('nome')->lists('nome', 'id')->toArray();
        $this->atributos = Atributo::orderBy('nome')->get();
        if(!empty($this->atributos)){
            foreach($this->atributos as $atributo){
                // add na string
                if(!empty($this->atributos_string)) $this->atributos_string .= ',';
                    $this->atributos_string .= $atributo->nome;
                // popula o array:
                $this->atributos_array[$atributo->id] = $atributo->nome;
            }
        }
    }

    
    public function index()
    {
        $registros = ProdutoFoto::all();
        $registros = configurar_status_toogle($registros, $this->caminho);
        // Configurando os toggles:
        foreach($registros as $registro){
            $registro->html_toggle_destaque = gerar_status_toggle( array('coluna' => 'destaque', 
                                                    'destaque' => $registro->destaque,
                                                    'texto_ativo' => 'Sim',
                                                    'ajax'        => true,
                                                    'ajax_url'    => $this->caminho.$registro->id.'/destaque/atualizar_status/',
                                                    'texto_inativo' => 'Não') );
        }
        return view($this->views.'.index',['registros'=>$registros],[
                    'titulo' => $this->titulo,
                    'caminho' => $this->caminho,
               ]);
    }

    public function create()
    {
        $this->inicia_models();
        $html_toggle = gerar_status_toggle( array('status' => 1) ); 
        $html_destaque_toggle = gerar_status_toggle( array('coluna' => 'destaque', 
                                                    'destaque' => 0,
                                                    'texto_ativo' => 'Sim',
                                                    'texto_inativo' => 'Não') );
        $html_preco_site_toggle          = gerar_status_toggle( array('coluna' => 'exibir_preco_site', 
                                                                      'exibir_preco_site' => '0',
                                                                      'texto_ativo' => 'Sim',
                                                                      'texto_inativo' => 'Não') );
        
        $atributos_valores = array();
        // colocando null para os atributos, ja que é um novo cadastro e não terá nada pra ele:
        foreach($this->atributos as $atributo){
            $atributos_valores[$atributo->id] = null;
        }
        return view($this->views.'.form',[
                    'titulo'        => $this->titulo,
                    'caminho'       => $this->caminho,
                    'html_toggle'   => $html_toggle,
                    'marcas'  => $this->marcas,
                    'categorias'    => $this->categorias,
                    'html_destaque_toggle' => $html_destaque_toggle,
                    'html_preco_site_toggle' => $html_preco_site_toggle,
                    'atributos' => $this->atributos,
                    'atributos_valores' => $atributos_valores,
                    'atributos_string'  => $this->atributos_string,
                    'atributos_array'   => $this->atributos_array,
               ]);
    }

    public function store(ProdutosFotosRequest $request)
    {

        $input = $request->all();
        if(!isset($input['status']))
            $input['status'] = 0;

        $c= ProdutoFoto::create($input);

        // Upload das fotos do ambienete:
        $this->upload_multiplo($input, $c->id);

        $request->session()->flash('alert-success', config('mensagens.registro_inserido'));
        return redirect($this->caminho.'create');
    }

    public function show($id)
    {
        $this->inicia_models();
        $registro = ProdutoFoto::find($id);
        $html_toggle = gerar_status_toggle( $registro );
        $html_destaque_toggle = gerar_status_toggle( array('coluna' => 'destaque', 
                                                    'destaque' => $registro->destaque,  
                                                    'texto_ativo' => 'Sim',
                                                    'texto_inativo' => 'Não') );
        return view($this->views.'.form', compact('registro'),[
                    'titulo'       => $this->titulo,
                    'caminho'      => $this->caminho,
                    'html_toggle'  => $html_toggle,
                    'marcas' => $this->marcas,
                    'categorias'   => $this->categorias,
                    'html_destaque_toggle' => $html_destaque_toggle,
               ]);
    }

    public function update(ProdutosRequest $request, $id)
    {
        $input = $request->all();
        if(!isset($input['status']))
            $input['status'] = 0;

        if(!isset($input['destaque']))
            $input['destaque'] = 0;

        if(isset($input['remover_arquivo'])){
            foreach($input['remover_arquivo'] as $arq)
                $input[$arq] = null;
        }

        // Upload das fotos do ambienete:
        $this->upload_multiplo($input, $id);

        // Remoção de fotos:
        $this->remover_fotos($input);

        $update = ProdutoFoto::find($id)->update($input);

        $request->session()->flash('alert-success', config('mensagens.registro_alterado'));
        return redirect($this->caminho.$id.'');
    }

    public function destroy($id)
    {
        ProdutoFoto::where('id', '=', $id)->delete();
        ProdutoFoto::find($id)->delete();
        return redirect($this->caminho);
    }

    public function upload_multiplo($input, $produto)
    {
        $fotos = [];
        // Para cada foto, salva no bd:
        if(isset($input['fotos'])){
            foreach($input['fotos'] as $foto){
                if($foto == null)
                    return false;
                $manipulador = new Arquivo;
                $file = $manipulador->add($foto);
                // recortar:
                $manipulador->recortar('produtos_fotos', 'arquivo_id', $file->id);

                $fotos[] = ['arquivo_id'  => $file->id,
                            'produto' => $produto,
                            'created_at'  =>date('Y-m-d H:i:s'),
                            'updated_at'  => date('Y-m-d H:i:s'),
                            ];
            }
        }
        if(count($fotos)){
            ProdutoFoto::insert($fotos);
        }
    }

    public function remover_fotos($input)
    {
        if(isset($input['remover_fotos']))
        {
            foreach($input['remover_fotos'] as $foto_id){
                $foto = ProdutoFotos::find($foto_id);
                // deleta da tabela ambientes_fotos:
                $foto->delete();
                // deleta da tabela arquivos:
                Arquivo::find($foto->arquivo_id)->delete();
            }
        }
    }

    public function marcar_capa($foto_id)
    {
        $foto = ProdutoFoto::findOrFail($foto_id);
        // Marca as fotos deste ambiente como false para principal:
        ProdutoFoto::where('produto', '=', $foto->produto)->update(['principal' => 0]);
        $foto->principal = 1;
        $foto->save();

        \Session::flash('alert-success', 'Foto principal atualizada');
        return redirect($this->caminho.$foto->produto.'');
    }

    // Atualiza um campo boolean de um registro via ajax
    public function atualizar_status($id, $coluna = 'status')
    {
        // Verifica o status atual e dá um update com o novo status:
        $registro = Produto::find($id);
        // Se encontrou o registro:
        if(isset($registro->{$coluna})){
            $novo = !$registro->{$coluna};
            $update = Produto::find($id)->update( array( $coluna =>$novo ) );
            $resposta['success'] = 'success';
            $resposta['status']  = '200';
        }else{
            $resposta['success'] = 'fail';
            $resposta['status']  = '0';
        }
        return \Response::json($resposta);
    }
 
}



