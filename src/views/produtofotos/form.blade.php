@extends('layouts.admin')

@section('content')
@if(!isset($registro->id))
    {!! Form::open(['url' => $caminho, 'files'=>true]) !!}
@else
    {!! Form::model($registro, ['url' => $caminho.$registro->id.'/update', 'method'=>'put', 'files'=>true]) !!}
@endif
<div class="row">
    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4 class="title">{{ $titulo }} <span class="pull-right">{!! $html_toggle !!}</span></h4>
            </div>
            <div class="content">
                    <div class="row">
                        <div class="col-sm-12">
                            <div class="form-group" >
                                {!! Form::label('destaque', 'Produto em destaque?') !!}
                                {!! $html_destaque_toggle !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-8">
                            <div class="form-group" >
                                {!! Form::label('nome', 'Nome:') !!}
                                {!! Form::text('nome', null, ['class' => 'form-control', 'autofocus'] ) !!}
                            </div>
                        </div>
                        <div class="col-sm-4">
                            <div class="form-group" >
                                {!! Form::label('sku', 'SKU:') !!}
                                {!! Form::text('sku', null, ['class' => 'form-control'] ) !!}
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('categoria', 'Categoria:') !!}
                                 {!! Form::select('categoria', $categorias, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                        <div class="col-sm-6">
                            <div class="form-group">
                                {!! Form::label('marca', 'Marca:') !!}
                                {!! Form::select('marca', $marcas, null, ['class' => 'form-control']) !!}
                            </div>
                        </div>
                    </div>
                    <div class="form-group" >
                        <div class="row">
                            <div class="col-sm-6">
                                {!! Form::label('preco', 'Preço no site (R$) :') !!}
                                {!! Form::number('preco', null, ['class' => 'form-control', 'min'=>0, 'step'=> 'any'] ) !!}
                            </div>
                            <div class="col-sm-6">
                                {!! Form::label('exibir_preco_site', 'Exibir no site?') !!}
                                {!! $html_preco_site_toggle !!}
                            </div>
                        </div>
                    </div>

                   <div class="form-group" >
                        {!! Form::label('keywords', 'Palavras-chave') !!}
                        {!! Form::text('keywords', null, ['class' => 'form-control', 'placeholder'=>'Digite as palavras-chave para este produto, separadas por vírgula'] ) !!}
                    </div>

                    <hr>
                    <div class="form-group" >
                        {!! Form::label('imagens[]', 'Upload de imagens:') !!}
                        {!! Form::file('imagens[]', array('multiple', 'accept'=>'image/*') ) !!}
                    </div>
                    <hr>

                <h4 class="title">Descrição/Informações</h4>

                    <div class="form-group" >
                    {!! Form::textarea('descricao', null, ['class' => 'form-control editor'] ) !!}
                </div>
                    
            </div>
        </div>
    </div>

    <div class="col-sm-12">
        <div class="card">
            <div class="header">
                <h4 class="title">Atributos</h4>
            </div>
            <div class="content">
                <table class="table table-striped">
                    <thead>
                        <tr>
                            <th>Atributo</th>
                            <th>Valor</th>
                            <th></th>
                        </tr>
                    </thead>
                    <tbody>
                    {{--
                        @forelse($atributos as $atributo)
                            @if(isset($atributos_valores[$atributo->id]))
                            <tr>
                                <td>{{$atributo->nome}}</td>
                                <td>
                                    {!! Form::text('attr_'.$atributo->id, $atributos_valores[$atributo->id], ['class' => 'form-control', 'id' => 'attr_'.$atributo->id] ) !!}
                                </td>
                                <td>
                                    <button type="button" class="btn btn-xs btn-danger btn-fill remover-atributo" id="remover-atributo-{{$atributo->nome}}" onclick="$('{{ '#attr_'.$atributo->id }}').val('')" data-toggle="tooltip" title="Remover"><i class="fa fa-times"></i></button>
                                </td>
                            </tr>
                            @endif
                        @empty
                            <tr>
                                <td colspan="3">
                                    Nenhum atributo encontrado
                                </td>
                            </tr>
                        @endforelse
                        --}}
                    </tbody>
                </table>
                <hr>
                <p>Adicionar um atributo</p>
                <!-- 
                    Para o autocomplete:
                    {{ $atributos_string }}
                    ou
                    $atributos_array (id => nome)
                -->
                <div class="form-group">
                    <div class="input-group novo-atributo">
                        <input type="text" name="novo_atributo_nome" class="form-control" placeholder="Atributo">
                        <input type="text" name="novo_atributo_valor" class="form-control" placeholder="Valor">
                        <span class="input-group-btn">
                            <button class="btn btn-success btn-fill"><i class="fa fa-plus"></i></button>
                        </span>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="col-sm-12">
        <div class="panel panel-default">
            <div class="panel-body">
                {!! Form::submit('Gravar', ['class' => 'btn btn-fill btn-wd btn-success pull-right']) !!}
            </div>
        </div>
    </div>


{!! Form::close() !!}
{{--
<!-- form apenas para as imagens: -->
@if( isset($imagens) && (count($imagens) > 0) )
{!! Form::open(['url' => $caminho.$registro->id.'/remover_arquivos', 'files'=>true]) !!}
        <div class="col-sm-12">
            <div class="card">
                <div class="header">
                    <h4 class="title">Imagens</h4>
                </div>
                <div class="content">
                    @foreach($imagens as $img)
                        <a href="{{ url('uploads/produtos/grande/'.$img->nome) }}" target="_blank" title="clique para ver em tamanho original">
                            {!! Html::image('uploads/produtos/medio/'.$img->nome, 'imagem', array('class'=>'img-responsive img-thumbnail', 'width'=>'100px' ) )!!}
                        </a>
                        <label><input type="checkbox" name="remover_arquivo[]" value="{{ $img->id }}" > Remover arquivo</label>
                    @endforeach
                    <hr>
                    <div class="form-group" >
                        {!! Form::submit('Remover', ['class' => 'btn btn-fill btn-wd btn-warning']) !!}
                    </div>
                </div>
            </div>
        </div>
{!! Form::close() !!}
</div>
@endif
--}}



@endsection
